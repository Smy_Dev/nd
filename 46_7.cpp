#include <iostream>

using namespace std;

int main()
{
    int n, jon = 0, pet = 0, r = 1;
    cin >> n;

    while(n > 0)
    {
        if(r % 2 == 1)
        {
            if(n - r > 0)
            {
                n -= r;
                jon += r;
                r++;
            }
            else
            {
                jon += n;
                n = 0;
            }

        }
        else
        {
            if(n - r > 0)
            {
                n -= r;
                pet += r;
                r++;
            }
            else
            {
                pet += n;
                n = 0;
            }
        }
    }
    cout << jon << endl << pet;
}
