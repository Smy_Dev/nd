#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    int x1, x2;
    fstream file("25_1.txt");
    file >> x1;
    file >> x2;
    file.close();

    int daug = x1;
    int is = 1;

    for(int i = x1; i <= x2; i++)
    {
        for(int j = x1; j <= x2; j++)
        if(i % j == 0)
        {
            cout << i << " + " << j << " = " << i + j << endl;
            cout << i << " - " << j << " = " << i - j << endl;
            cout << i << " * " << j << " = " << i * j << endl;
            cout << i << " / " << j << " = " << i / j << endl;
            cout << "************" << endl;
        }
    }

    return 0;
}
